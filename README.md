# OpenML dataset: parkinson-speech-uci

https://www.openml.org/d/42176

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Source:

C. Okan Sakar a, Gorkem Serbes b, Aysegul Gunduz c,
Hunkar C. Tunc a, Hatice Nizam d, Betul Erdogdu Sakar e, Melih Tutuncu c,
Tarkan Aydin a, M. Erdem Isenkul d, Hulya Apaydin c
a Department of Computer Engineering, Bahcesehir University, Istanbul, 34353, Turkey
b Department of Biomedical Engineering, Yildiz Technical University, Istanbul, 34220, Turkey
c Department of Neurology, CerrahpaAYa Faculty of Medicine, Istanbul University, Istanbul 34098, Turkey
d Department of Computer Engineering, Istanbul University, Istanbul, 34320, Turkey
e Department of Software Engineering, Bahcesehir University, Istanbul, 34353, Turkey
e-mails: {okan.sakar '@' eng.bau.edu.tr; gserbes '@' yildiz.edu.tr;
draysegulgunduz '@' yahoo.com; hunkarcan.tunc '@' stu.bahcesehir.edu.tr; haticenizam92 '@' gmail.com; betul.erdogdu '@' eng.bau.edu.tr; tutuncumelih '@' yahoo.com; tarkan.aydin '@' eng.bau.edu.tr; eisenkul '@' istanbul.edu.tr; hulyapay '@' istanbul.edu.tr}


Data Set Information:

The data used in this study were gathered from 188 patients with PD (107 men and 81 women) with ages ranging from 33 to 87 (65.1A+-10.9) at the Department of Neurology in CerrahpaAYa Faculty of Medicine, Istanbul University. The control group consists of 64 healthy individuals (23 men and 41 women) with ages varying between 41 and 82 (61.1A+-8.9). During the data collection process, the microphone is set to 44.1 KHz and following the physicianaEUR(tm)s examination, the sustained phonation of the vowel /a/ was collected from each subject with three repetitions.


Attribute Information:

Various speech signal processing algorithms including Time Frequency Features, Mel Frequency Cepstral Coefficients (MFCCs), Wavelet Transform based Features, Vocal Fold Features and TWQT features have been applied to the speech recordings of Parkinson's Disease (PD) patients to extract clinically useful information for PD assessment.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42176) of an [OpenML dataset](https://www.openml.org/d/42176). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42176/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42176/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42176/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

